# Code Challenge

## Requirements

- [Node v16.13.2](https://nodejs.org)

### Setup
---
1. Run `npm install`
2. For Phase 1, run `npm start phase1`
3. For Phase 2A, run `npm start phase2a`
4. For Phase 2B, run `npm start phase2b`
5. For Phase 2C, run `npm start phase2c`
6. For Phase 2D, run `npm run test`
