import Phase2c from './phase2c.js';

describe('testing Phase2C', () => {
  let phase2c;

  beforeAll(() => {
   phase2c = new Phase2c();
  })

  test('Get random number', () => {
    expect(phase2c.randomNumber()).toBeGreaterThanOrEqual(1);
    expect(phase2c.randomNumber()).toBeLessThanOrEqual(12);
    expect(phase2c.randomNumber()).not.toBe(0);
    expect(phase2c.randomNumber()).not.toBe(12);
  });
});