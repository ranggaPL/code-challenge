import Phase2b from './phase2b.js';

describe('testing Phase2b', () => {
  let phase2b;

  beforeAll(() => {
   phase2b = new Phase2b();
  })

  test('Input Recite Number 1 the result must be "This is the house"', () => {
    expect(phase2b.reciteLyrics(1)).toBe("This is the house");
  });

  test('Input Recite Number 2 the result must be "This is the malt and the house"', () => {
    expect(phase2b.reciteLyrics(2)).toBe("This is the malt and the house");
  });

  test('Input Recite Number 3 the result must be "This is the rat, the malt and the house"', () => {
    expect(phase2b.reciteLyrics(3)).toBe("This is the rat, the malt and the house");
  });

  test('Input Recite Number foo the result must be "Recite Number must be a number."', () => {
    expect(phase2b.reciteLyrics("foo")).toEqual("Recite Number must be a number.");
  });

  test(`Input Recite Number 0 the result must be "Recite Number must be start from 1 to 12."`, () => {
    expect(phase2b.reciteLyrics(0)).toEqual(`Recite Number must be start from 1 to 12.`);
  });

  test(`Input Recite Number 20 the result must be "Recite Number must be start from 1 to 12."`, () => {
    expect(phase2b.reciteLyrics(20)).toEqual(`Recite Number must be start from 1 to 12.`);
  });
});