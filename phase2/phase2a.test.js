import Phase2a from './phase2a.js';

describe('testing Phase2A', () => {
  let phase2a;

  beforeAll(() => {
   phase2a = new Phase2a();
  })

  test('Get random number', () => {
    expect(phase2a.randomNumber()).toBeGreaterThanOrEqual(1);
    expect(phase2a.randomNumber()).toBeLessThanOrEqual(12);
    expect(phase2a.randomNumber()).not.toBe(0);
    expect(phase2a.randomNumber()).not.toBe(12);
  });
});