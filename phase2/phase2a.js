import Phase1 from './../phase1/phase1.js';

class Phase2a extends Phase1 {
  constructor() {
    super();
    this.minNumber = 1;
    this.maxNumber = this.totalRows;
  }

  start() {
    let randomNumber = this.randomNumber();
    let result = this.reciteLyrics(randomNumber);
    this.print(result);
  }

  randomNumber() {
    return Math.floor(Math.random() * (this.maxNumber - this.minNumber + 1)) + this.minNumber;
  }
}

export default Phase2a;
