import Phase2b from './phase2b.js';

class Phase2c extends Phase2b {
  constructor() {
    super();
    this.minNumber = 1;
    this.maxNumber = this.totalRows;
  }

  start() {
    let randomNumber = this.randomNumber();
    let result = this.reciteLyrics(randomNumber);
    this.print(result);
  }

  randomNumber() {
    return Math.floor(Math.random() * (this.maxNumber - this.minNumber + 1)) + this.minNumber;
  }
}

export default Phase2c;
