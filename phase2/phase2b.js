import Phase1 from './../phase1/phase1.js';

class Phase2b extends Phase1 {
  constructor() {
    super();
  }

  reciteLyrics(reciteNumber) {
    let { isValid, error } = this.validateReciteNumber(reciteNumber);
    if (!isValid) {
      return error;
    }
    
    let result;
    let lastIndex = this.totalRows;
    let startIndex = this.totalRows - reciteNumber;
    let selectedLyricsArr = this.lyricsPerRow.slice(startIndex, lastIndex);
    let selectedSubjectsArr = selectedLyricsArr.map(value => value.split(' ').slice(0,2).join(' '));
    if (selectedSubjectsArr.length > 1) {
      let lastLyric = selectedSubjectsArr.pop();
      result = selectedSubjectsArr.join(", ") + " and " + lastLyric;
    }
    else {
      result = selectedSubjectsArr.toString();
    }

    return "This is " + result;
  }
}

export default Phase2b;