import Phase1 from './phase1/phase1.js';
import Phase2a from './phase2/phase2a.js';
import Phase2b from './phase2/phase2b.js';
import Phase2c from './phase2/phase2c.js';

const args = process.argv.slice(2);

switch (args[0]) {
  case 'phase1':
    let phase1 = new Phase1();
    phase1.start();
    break;
  case 'phase2a':
    let phase2a = new Phase2a();
    phase2a.start();
    break;
  case 'phase2b':
    let phase2b = new Phase2b();
    phase2b.start();
    break;
  case 'phase2c':
    let phase2c = new Phase2c();
    phase2c.start();
    break;
  default:
    console.log('Sorry, that is not something I know how to do.');
}