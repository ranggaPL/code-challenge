import * as fs from 'fs';

class Base {

  constructor(){
    this.lyricsPerRow = [];
    this.totalRows = 0;
    this.init()
  }

  init(){
    let content = fs.readFileSync('./base/lyrics.txt', {encoding:'utf8', flag:'r'});
    this.lyricsPerRow = content.split('\n');
    this.totalRows = this.lyricsPerRow.length;
  }
}

export default Base;