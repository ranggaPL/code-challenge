import readline from 'readline';
import { stdin as input, stdout as output } from 'process';

import Base from './../base/base.js';

class Phase1 extends Base {
  
  constructor() {
    super();
  }

  start() {
    this.rl = readline.createInterface({ input, output });
    this.askReciteNumber();
  }

  reciteLyrics(reciteNumber) {
    let { isValid, error } = this.validateReciteNumber(reciteNumber);
    if (!isValid) {
      return error;
    }

    let lastIndex = this.totalRows;
    let startIndex = this.totalRows - reciteNumber;
    let selectedLyricsArr = this.lyricsPerRow.slice(startIndex, lastIndex);
    
    return "This is " + selectedLyricsArr.join(" ");
  }

  validateReciteNumber(reciteNumber) {
    let isValid = true;
    let error = "";

    if (isNaN(reciteNumber)) {
      isValid = false;
      error = "Recite Number must be a number.";
    } else if (reciteNumber === 0 || reciteNumber > this.totalRows) {
      isValid = false;
      error = `Recite Number must be start from 1 to ${this.totalRows}.`;
    }

    return { isValid, error };
  }

  askReciteNumber() {
    this.rl.question('Input Recite Number: ', (input) => {
      let result = this.reciteLyrics(input);
      this.print(result);

      // ask the recite number again
      this.askReciteNumber();
    });
  }

  print(result) {
    console.log(result);
  }
}

export default Phase1;
