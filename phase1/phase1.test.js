import Phase1 from './phase1.js';

describe('testing Phase1', () => {
  let phase1;

  beforeAll(() => {
   phase1 = new Phase1();
  })

  test('Input Recite Number 1 the result must be "This is the house that Jack built"', () => {
    expect(phase1.reciteLyrics(1)).toBe("This is the house that Jack built");
  });

  test('Input Recite Number 2 the result must be "This is the malt that lay in the house that Jack built"', () => {
    expect(phase1.reciteLyrics(2)).toBe("This is the malt that lay in the house that Jack built");
  });

  test('Input Recite Number 12 the result must be "This is the horse and the hound and the horn that belonged to the farmer sowing his corn that kept the rooster that crowed in the morn that woke the priest all shaven and shorn that married the man all tattered and torn that kissed the maiden all forlorn that milked the cow with the crumpled horn that tossed the dog that worried the cat that killed the rat that ate the malt that lay in the house that Jack built"', () => {
    expect(phase1.reciteLyrics(12)).toBe("This is the horse and the hound and the horn that belonged to the farmer sowing his corn that kept the rooster that crowed in the morn that woke the priest all shaven and shorn that married the man all tattered and torn that kissed the maiden all forlorn that milked the cow with the crumpled horn that tossed the dog that worried the cat that killed the rat that ate the malt that lay in the house that Jack built");
  });

  test('Input Recite Number foo the result must be "Recite Number must be a number."', () => {
    expect(phase1.reciteLyrics("foo")).toEqual("Recite Number must be a number.");
  });

  test(`Input Recite Number 0 the result must be "Recite Number must be start from 1 to 12."`, () => {
    expect(phase1.reciteLyrics(0)).toEqual(`Recite Number must be start from 1 to 12.`);
  });

  test(`Input Recite Number 20 the result must be "Recite Number must be start from 1 to 12."`, () => {
    expect(phase1.reciteLyrics(20)).toEqual(`Recite Number must be start from 1 to 12.`);
  });
});